# DraftElec
DraftElec API Documentation

Read the documentation at [this link](https://draftelec-api-documentation.readthedocs.io/en/latest/)


[![Documentation Status](https://readthedocs.org/projects/draftelec-api-documentation/badge/?version=latest)](https://draftelec-api-documentation.readthedocs.io/en/latest/?badge=latest)

