.. DraftElec Documentation documentation master file, created by
    sphinx-quickstart on Tue Jan 30 23:02:04 2018.
    You can adapt this file completely to your liking, but it should at least
    contain the root `toctree` directive.

Welcome to DraftElec's documentation
====================================
.. image:: src/img/logo.png
By : Mourad Arifi
Email : arifi.armedia@gmail.com

Build Motor Controller Center / Single Lignes Autocad drawings based on Json specifications

Guide
^^^^^
.. toctree::
    :maxdepth: 2
    
    src/requirements
    src/installation
    src/quick-starter-guide
    src/licence

Reviews

^^^^^^^

.. image:: src/img/support.jpg

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

